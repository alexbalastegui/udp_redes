#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <Definition.h>
#include "MyButton.h"
#include <thread>

enum class action {
	_logIn,
	_signUp,
	_none,
	_MAX
};

class Lobby
{
public:
	Lobby(int _scene, std::string _username);
	~Lobby();

	void mainLoop();
	bool lobbyIsRunning = true;
	bool *close;

	sf::UdpSocket socket;

	bool criticalOn=false;
	void goCritical(sf::Packet _criticalPacket);
	int gameID = -1;

	int scene = 0;
	action authenticathion=action::_none;
	bool lookingForGame = false;
	bool allGood = false;

	sf::RenderWindow* window;
	sf::Texture bg;
	sf::Sprite bgSprite;

	std::string username = "SampleUser";
	std::string password = "SamplePass";

	MyButton* playButton;
	MyButton* exitButton;
	MyButton* logInButton;
	MyButton* signUpButton;

	sf::Sprite* logo;
	sf::Texture logoText;

	sf::Sprite* buffering;
	sf::Texture bufferingText;

	sf::Text* usernameDisplay;
	sf::Font font;

private:
	void closeLobby(bool exitApp);

	void launchAuthenticathionThread();
	void authenticathionThread(action* _authenticathion, std::string *_username, std::string *_password, int* _scene, sf::UdpSocket* _socket);

	void launchHearingThread();
	void hearingThread(sf::UdpSocket* _socket, bool* _lobbyIsRunning, bool *_criticalOn, int* _gameId, bool* _allGood, int*_scene);

	void launchCriticalThread(sf::Packet _criticalPacket);
	void criticalThread(sf::Packet _criticalPacket, bool* _criticalOn, sf::UdpSocket *_socket);

};

