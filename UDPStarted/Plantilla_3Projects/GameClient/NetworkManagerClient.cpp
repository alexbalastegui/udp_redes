#include "NetworkManagerClient.h"


std::mutex mutex1;
std::mutex enemyMutex;

NetworkManagerClient::NetworkManagerClient(int _gameID, std::string _username)
{
	username = _username;
	gameID = _gameID;
	inputQ.gameID = gameID;


	//No har� nada hasta que se cargue su valor al enviar el welcome el server
	me = new bg::Player;

	LaunchCriticalThread();
	LaunchHearingThread();

	sf::Packet helloPacket;
	helloPacket << bg::C2SProt::HELLO;
	helloPacket << gameID;
	criticalQ.push({ helloPacket, bg::S2CProt::WELCOME });

	std::cout << "Port: " << socket.getLocalPort() << " with gameID: " << gameID << std::endl;

	sf::Image icon;
	icon.loadFromFile(bg::IconPath);
	window = new sf::RenderWindow(sf::VideoMode(bg::windowX, bg::windowY), bg::gameName);
	window->setIcon(512, 512, icon.getPixelsPtr());


	font.loadFromFile(bg::LobbyButtonFont);
	youAre.setFont(font);
	youAre.setPosition({10, 10});
	youAre.setString(username);

	lobbyButton = new MyButton({55, 80}, "Lobby");
	exitButton = new MyButton({55, 190}, "Exit");


	//El gameID se actualiza despu�s del constructor
}


NetworkManagerClient::~NetworkManagerClient()
{
}

void NetworkManagerClient::LaunchCriticalThread()
{
	std::thread criticalThread(&NetworkManagerClient::CriticalThread, this, &criticalQ, &socket);
	criticalThread.detach();
}

void NetworkManagerClient::CriticalThread(std::queue<bg::CritPacket_C2S> *_criticalQ, sf::UdpSocket* _socket)
{
	sf::Clock criticalClock;
	while (appIsRunning) {
		while (!_criticalQ->empty() && appIsRunning) {
			if (criticalClock.getElapsedTime().asSeconds() >= bg::CriticalWait) {
				_socket->send(_criticalQ->front().packet, bg::serverIP, bg::serverPort);
				criticalClock.restart();
			}
		}
	}
}

void NetworkManagerClient::LaunchHearingThread()
{
	std::thread hearingThread(&NetworkManagerClient::HearingThread, this, &socket, &criticalQ, &players, window, &inputQ, &appIsRunning, &youAre, &enemies, &gameFinished, &scene, &powerUps);
	hearingThread.detach();
}

void NetworkManagerClient::HearingThread
(sf::UdpSocket* _socket, std::queue<bg::CritPacket_C2S> *_criticalQ, std::map<int, bg::Player> *_players, sf::RenderWindow* _window, bg::MovementQueuer* _inputQ, 
	bool* _appIsRunning, sf::Text* _youAre, std::deque<bg::Enemy>* _enemies, bool* _gameFinished, int*_scene, std::deque<bg::PowerUp>* _powerups)
{
	while (appIsRunning) {
		sf::Packet hearedPacket;
		sf::IpAddress hearedIP;
		unsigned short hearedPort;
		if (_socket->receive(hearedPacket, hearedIP, hearedPort) == sf::Socket::Status::Done) { //Recibe de cualquier ip y puerto
			if (hearedPort==bg::serverPort && hearedIP == bg::serverIP) { //Nos aseguramos de que lo recibido ha sido enviado por el servidor de nuestro juego

				float rndPacketLoss = bg::randomFloat();
				if (rndPacketLoss < bg::PERCENT_PACKETLOSS) {
					bg::S2CProt prot;
					hearedPacket >> prot;
					std::cout << "Simulamos que se ha perdido el mensaje " << static_cast<int>(prot) << std::endl;
					hearedPacket.clear();
				}

				bg::S2CProt hearedCode;
				hearedPacket >> hearedCode;
				switch (hearedCode) {
				case bg::S2CProt::WELCOME: {
					if (!_criticalQ->empty()) {
						if (_criticalQ->front().ackCode == bg::S2CProt::WELCOME)
							_criticalQ->pop();
					}

					bg::Player temp;
					hearedPacket >> temp;
					(*_players)[temp.getPlayerID()] = temp;
					
					me = &(*_players)[temp.getPlayerID()]; //Hacemos que el puntero  me  apunte al elemento del mapa players que corresponde al jugador del cliente
					me->inputQueuer = _inputQ;


					
					_youAre->setFillColor(me->idToColor());

					std::cout << "I am connected! I am: " << _socket->getLocalPort() << std::endl;
				}
					break;
				case bg::S2CProt::NEWPLAYER: {
					std::cout << "Received new player" << std::endl;
					bg::Player newPlayer;
					hearedPacket >> newPlayer;
					(*_players)[newPlayer.getPlayerID()] = newPlayer;
					(*_players)[newPlayer.getPlayerID()].setFollowablePos((*_players)[newPlayer.getPlayerID()].getPosition());
				}
					break;
				case bg::S2CProt::OKMOVE: {
					bg::Movement confirmedMovement;
					int movedPlayerID=-1;
					hearedPacket >> movedPlayerID >> confirmedMovement;
					if (movedPlayerID == me->getPlayerID()) {
						mutex1.lock();
						_inputQ->acknowledgeOrder(confirmedMovement, &(*_players)[movedPlayerID]);
						mutex1.unlock();
					}
					else {
						(*_players)[movedPlayerID].setFollowablePos(confirmedMovement.abs);
					}
					//(*_players)[movedPlayerID].setPosition(confirmedMovement.abs);
					//std::cout << "New mov: " << (*_players)[movedPlayerID]->getPosition().x << " - " << (*_players)[movedPlayerID]->getPosition().y << std::endl;
				}
					break;
				case bg::S2CProt::PING: {
					sf::Packet pongPacket;
					pongPacket << bg::C2SProt::PONG << me->getPlayerID();
					_socket->send(pongPacket, bg::serverIP, bg::serverPort);
				}
					break;
				case bg::S2CProt::PLAYERDISCONNECTED: {
					int disconnectedPlayerID = -1;
					hearedPacket >> disconnectedPlayerID;
					_players->erase(disconnectedPlayerID);
				}
					break;
				case bg::S2CProt::OKDISCONNECT: {
					if (_criticalQ->front().ackCode == bg::S2CProt::OKDISCONNECT && !_criticalQ->empty())
						_criticalQ->pop();
					_window->close();
					_appIsRunning = false;
				}
					break;
				case bg::S2CProt::ACK_S2C:
					break;
				case bg::S2CProt::ENEMIES: {
					enemyMutex.lock();
					int enemyAmount = 0;
					hearedPacket >> enemyAmount;
					_enemies->clear();
					_powerups->clear();
					for (int i = 0; i < enemyAmount; ++i) {
						bg::Enemy tempEnemy;
						hearedPacket >> tempEnemy;
						_enemies->push_back(tempEnemy);
					}
					int powerUpAmount = 0;
					hearedPacket >> powerUpAmount;

					for (int i = 0; i < powerUpAmount; ++i) {
						bg::PowerUp tempPowerUp;
						hearedPacket >> tempPowerUp;
						_powerups->push_back(tempPowerUp);
					}

					hearedPacket >> *_gameFinished;

					
					if (*_gameFinished) {
						int deadID = -1;
						*_scene = 1;
						hearedPacket >> deadID;
						(*_players)[deadID].isAlive = false;
						if (me->isAlive) {
							_youAre->setString("You win");
							_youAre->setFillColor(sf::Color::White);
						}
						else {
							_youAre->setString("You lose");
							_youAre->setFillColor(sf::Color::Black);
						}
					}
					
					enemyMutex.unlock();
				}
					break;
				case bg::S2CProt::POWERUP: {
					bg::PowerUpType kind;
					int affectedID=-1;
					float val = 0;
					hearedPacket >> kind >> affectedID >> val;
					switch (kind) {
					case bg::PowerUpType::SizeDown:
						(*_players)[affectedID].setSize((*_players)[affectedID].sizeScalar * val);
						break;
					case bg::PowerUpType::SizeUp:
						(*_players)[affectedID].setSize((*_players)[affectedID].sizeScalar * val);
						break;
					case bg::PowerUpType::SpeedDown:
						(*_players)[affectedID].setSpeed((*_players)[affectedID].speedScalar * val);
						break;
					case bg::PowerUpType::SpeedUp:
						(*_players)[affectedID].setSpeed((*_players)[affectedID].speedScalar * val);
						break;
					}
				}
					break;
				}
			}

		}
	}
}

void NetworkManagerClient::MainLoop()
{
	sf::Clock mainClock;
	while (appIsRunning) {
			switch (scene){

			case 0: {
				//Event handler
				sf::Event evnt;
				while (window->pollEvent(evnt)) {
					switch (evnt.type) {
					case sf::Event::Closed:
						sf::Packet closePacket;
						closePacket << bg::C2SProt::DISCONNECT << me->getPlayerID();
						criticalQ.push({ closePacket, bg::S2CProt::OKDISCONNECT });
						break;
					}
				}

				//Update
				for (auto it = players.begin(); it != players.end(); ++it) {
					if (it->second.getPlayerID() != me->getPlayerID())
						it->second.setPositionLerp(bg::InterpolationAlpha);
				}

				if (window->hasFocus()) {
					mutex1.lock();
					me->Update(mainClock.getElapsedTime().asSeconds(), &socket);
					mutex1.unlock();

					mainClock.restart();
				}
				//Draw

				for (auto it = players.begin(); it != players.end(); ++it)
					it->second.Draw(window);

				enemyMutex.lock();
				for (auto it = enemies.begin(); it != enemies.end(); ++it) {
					it->Draw(window);
				}
				for (auto it = powerUps.begin(); it != powerUps.end(); ++it) {
					it->Draw(window);
				}
				enemyMutex.unlock();

				window->draw(youAre);

				window->display();
				window->clear(bg::bgColor);
				
			}
					break;
			case 1: {
				//Event handler
				sf::Event evnt;
				while (window->pollEvent(evnt)) {
					switch (evnt.type) {
					case sf::Event::Closed:
						sf::Packet closePacket;
						closePacket << bg::C2SProt::DISCONNECT << me->getPlayerID();
						criticalQ.push({ closePacket, bg::S2CProt::OKDISCONNECT });
						break;
					}
					lobbyButton->EventHandling(&evnt, window);
					exitButton->EventHandling(&evnt, window);
				}

				//Update
				lobbyButton->Update();
				exitButton->Update();

				if (lobbyButton->isClicked) {
					lobbyScene = 1;

					sf::Packet closePacket;
					closePacket << bg::C2SProt::DISCONNECT << me->getPlayerID();
					criticalQ.push({ closePacket, bg::S2CProt::OKDISCONNECT });
				}
				if (exitButton->isClicked) {
					sf::Packet closePacket;
					closePacket << bg::C2SProt::DISCONNECT << me->getPlayerID();
					criticalQ.push({ closePacket, bg::S2CProt::OKDISCONNECT });
				}

				//Draw

				window->draw(youAre);
				lobbyButton->Draw(window);
				exitButton->Draw(window);

				window->display();
				window->clear(sf::Color::White - bg::bgColor);
			}
					break;

		}
	}
}
