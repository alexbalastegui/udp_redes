#pragma once
#include <Definition.h>

#include <SFML\Network.hpp>
#include <SFML\Graphics.hpp>
#include <iostream>
#include <map>
#include <queue>
#include <thread>
#include <mutex>
#include "MyButton.h"

class NetworkManagerClient
{
public:
	NetworkManagerClient(int _gameID, std::string _username);
	~NetworkManagerClient();

	bool appIsRunning = true;

	//El socket
	sf::UdpSocket socket;

	//Los jugadores
	std::map<int, bg::Player> players;
	bg::Player* me; //Apunta al elemento del mapa players que corresponde con el jugador local
	std::string username;

	//Id de la partida
	int gameID=-1;

	bg::MovementQueuer inputQ;
	
	//Los mensajes cr�ticos se reenviar�n hasta que el servidor haya confirmado su recepci�n
	void LaunchCriticalThread();
	void CriticalThread(std::queue<bg::CritPacket_C2S> *_criticalQ, sf::UdpSocket* _socket);
	std::queue<bg::CritPacket_C2S> criticalQ; 

	//El thread de escucha recibir� todos los mensajes y los interpretar�
	void LaunchHearingThread();
	void HearingThread(sf::UdpSocket* _socket, std::queue<bg::CritPacket_C2S> *_criticalQ, std::map<int, bg::Player> *_players, 
		sf::RenderWindow* _window, bg::MovementQueuer* _inputQ, bool* _appIsRunning, sf::Text* _youAre, std::deque<bg::Enemy>* _enemies, bool* _gameFinished, int*_scene, std::deque<bg::PowerUp>* _powerups);

	//MainLoop
	void MainLoop();
	bool gameFinished = false;
	int scene = 1;

	//Graphics
	sf::RenderWindow *window;
	sf::Text youAre;
	sf::Font font;

	MyButton* lobbyButton;
	MyButton* exitButton;

	//Enemies
	std::deque<bg::Enemy> enemies;
	std::deque<bg::PowerUp> powerUps;

	int lobbyScene = 0;

};

