#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>
#include <Definition.h>

class MyButton
{
public:
	MyButton();
	MyButton(sf::Vector2f _pos, std::string _message);
	~MyButton();

	bool isClicked=false;
	bool isHovered=false;
	bool canBeClicked = true;

	std::string getMessage();
	void setMessage(std::string _message);

	void EventHandling(sf::Event* _evnt, sf::RenderWindow* _window);
	void Update();
	void Draw(sf::RenderWindow* _window);

private:
	bool isClickedDecor = false;
	sf::Sprite drawable;
	sf::Texture texture;
	sf::Font font;
	sf::Text message;

};

