#include "MyButton.h"



MyButton::MyButton()
{
	texture.loadFromFile(bg::LobbyButtonPath);
	drawable.setTexture(texture);
}

MyButton::MyButton(sf::Vector2f _pos, std::string _message)
{
	texture.loadFromFile(bg::LobbyButtonPath);
	drawable.setTexture(texture);
	drawable.setPosition(_pos);

	font.loadFromFile(bg::LobbyButtonFont);
	message.setFont(font);
	message.setString(_message);
	message.setCharacterSize(50);
	message.setFillColor(sf::Color::Black);
	message.setPosition({ _pos.x + drawable.getLocalBounds().width/2 - message.getLocalBounds().width/2, _pos.y + message.getLocalBounds().height / 2 });
}


MyButton::~MyButton()
{
}

std::string MyButton::getMessage()
{
	return message.getString();
}

void MyButton::setMessage(std::string _message)
{
	message.setString(_message);

}

void MyButton::EventHandling(sf::Event * _evnt, sf::RenderWindow* _window)
{
	if (canBeClicked) {
		float mX = static_cast<float>(sf::Mouse::getPosition(*_window).x);
		float mY = static_cast<float>(sf::Mouse::getPosition(*_window).y);
		if ((mX > drawable.getPosition().x && mX < (drawable.getPosition().x + drawable.getLocalBounds().width)) &&
			(mY > drawable.getPosition().y && mY < (drawable.getPosition().y + drawable.getLocalBounds().height))) {
			isHovered = true;
			//Mouse is within bounds
			if (_evnt->type == sf::Event::MouseButtonPressed) {
				isClickedDecor = true;
			}
			else if (_evnt->type == sf::Event::MouseButtonReleased) {
				isClicked = true;
				isClickedDecor = false;
			}
		}
		else {
			isHovered = false;
			isClickedDecor = false;
		}
	}
}

void MyButton::Update()
{
	if (isClickedDecor) {
		drawable.setColor(bg::clickedColor);
		message.setFillColor(bg::clickedColor);
	}
	else if (isHovered) {
		drawable.setColor(bg::hoveredColor);
		message.setFillColor(bg::hoveredColor);
	}
	else {
		drawable.setColor(sf::Color::White);
		message.setFillColor(sf::Color::Black);
	}
}

void MyButton::Draw(sf::RenderWindow * _window)
{
	_window->draw(drawable);
	_window->draw(message);

	isClicked = false;
}
