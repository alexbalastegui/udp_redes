#include "Lobby.h"


Lobby::Lobby(int _scene, std::string _username)
{
	scene = _scene;
	if (scene == 1) {
		username = _username;
	}

	window = new sf::RenderWindow(sf::VideoMode(bg::lobbyWindowX, bg::lobbyWindowY), bg::gameName + " lobby");
	bg.loadFromFile(bg::LobbyBgPath);
	bgSprite.setTexture(bg);

	sf::Image icon;
	icon.loadFromFile(bg::IconPath);
	window->setIcon(512, 512, icon.getPixelsPtr());

	playButton = new MyButton({162.f, 225}, "Play");
	exitButton = new MyButton({162.f, 390}, "Exit");
	signUpButton = new MyButton({ 162.f, 280 }, "Sign Up");
	logInButton = new MyButton({ 162.f, 170 }, "Log In");

	logoText.loadFromFile(bg::LobbyGameLogo);
	logo = new sf::Sprite;
	logo->setTexture(logoText);
	logo->setPosition({ 72, 40 });

	bufferingText.loadFromFile(bg::BufferingPath);
	buffering = new sf::Sprite(bufferingText);
	buffering->setOrigin({50, 50});
	buffering->setPosition({ 162.f+350, 255+20 });

	font.loadFromFile(bg::LobbyButtonFont);
	usernameDisplay = new sf::Text(username, font);
	usernameDisplay->setCharacterSize(20);
	usernameDisplay->setPosition({5, 5});
	usernameDisplay->setFillColor(sf::Color::White);
	if (scene == 1) {
		usernameDisplay->setString(username);
	}

	launchHearingThread();
	std::cout << "Port: " << socket.getLocalPort() << std::endl;
}


Lobby::~Lobby()
{
}

void Lobby::mainLoop()
{
	while (lobbyIsRunning) {
		while (lobbyIsRunning && window->isOpen()) {
			switch (scene){
				case 0: {
					//Event handling
					sf::Event evnt;
					while (window->pollEvent(evnt)) {
						switch (evnt.type) {
						case sf::Event::Closed:
							closeLobby(true);
							break;
						}
						logInButton->EventHandling(&evnt, window);
						signUpButton->EventHandling(&evnt, window);
						exitButton->EventHandling(&evnt, window);
					}
					//Update

					if (logInButton->isClicked) {
						authenticathion = action::_logIn;
						logInButton->canBeClicked = false;
						signUpButton->canBeClicked = false;
						launchAuthenticathionThread();
					}
					if (signUpButton->isClicked) {
						authenticathion = action::_signUp;
						logInButton->canBeClicked = false;
						signUpButton->canBeClicked = false;
						launchAuthenticathionThread();
					}
					if (exitButton->isClicked) {
						closeLobby(true);
					}

					logInButton->Update();
					signUpButton->Update();
					exitButton->Update();

					//Draw
					window->clear();

					window->draw(bgSprite);
					logInButton->Draw(window);
					signUpButton->Draw(window);
					exitButton->Draw(window);
					window->draw(*logo);

					window->display();
				}
						break;
				case 1: {
					//Event Handling
					sf::Event evnt;
					while (window->pollEvent(evnt)) {
						switch (evnt.type) {
						case sf::Event::Closed:
							closeLobby(true);
							break;
						}
						playButton->EventHandling(&evnt, window);
						exitButton->EventHandling(&evnt, window);
					}
					//Update
					if (playButton->isClicked) {
						sf::Packet readyPacket;
						readyPacket << bg::C2SProt::LOBBY_READY;
						goCritical(readyPacket);
						playButton->canBeClicked = false;
						lookingForGame = true;
					}
					if (exitButton->isClicked) {
						closeLobby(true);
					}
					if (lookingForGame) {
						buffering->rotate(0.05f);
					}

					playButton->Update();
					exitButton->Update();
					if (allGood)
						closeLobby(false);

					//Draw
					window->clear();

					window->draw(bgSprite);
					playButton->Draw(window);
					exitButton->Draw(window);
					if(lookingForGame)
						window->draw(*buffering);
					window->draw(*logo);
					window->draw(*usernameDisplay);

					window->display();
				}
					break;
			}
		}
	}
}

void Lobby::goCritical(sf::Packet _criticalPacket)
{
	criticalOn = true;
	launchCriticalThread(_criticalPacket);
}

void Lobby::closeLobby(bool exitApp)
{
	*close = exitApp;
	window->close();
	lobbyIsRunning = false;
}

void Lobby::launchAuthenticathionThread()
{
	std::thread authenthication(&Lobby::authenticathionThread, this, &authenticathion, &username, &password, &scene, &socket);
	authenthication.detach();
}


void Lobby::authenticathionThread(action* _authenticathion, std::string *_username, std::string *_password, int* _scene, sf::UdpSocket* _socket)
{
	switch (*_authenticathion) {
	case action::_logIn: {
		std::cout << "--LogIn--" << std::endl;
		std::cout << "Username:" << std::endl;
		std::getline(std::cin, *_username);
		std::cout << "\nPassword:" << std::endl;
		std::getline(std::cin, *_password);

		sf::Packet pack;
		pack << bg::C2SProt::LOGIN << -1 << username << password;

		_socket->send(pack, bg::serverIP, bg::serverPort);

		//Comprobar si el username y la password son v�lidos. Si es as�, imprimir algo en plan "Logeado con �xito", y si no imprime "El usename o la contrase�a no coinciden" y haz goto logIn
	}
		break;
	case action::_signUp:{
		std::cout << "--SignUp--" << std::endl;
		std::cout << "Username:" << std::endl;
		std::getline(std::cin, *_username);
		std::cout << "\nPassword:" << std::endl;
		std::getline(std::cin, *_password); 

		sf::Packet pack;
		pack << bg::C2SProt::SIGNUP << -1 << username << password;
		_socket->send(pack, bg::serverIP, bg::serverPort);
		//Comprobar si el username es v�lido. Si lo es imprimir algo en plan "Cuenta creada con �xito" y si no lo es imprime "Username no disponible" y haz goto signUp para que vuelva a introducir sus datos
	}
		break;
	}
	//Username and Password debug
	//std::cout << *_username << " - " << *_password << std::endl;
	usernameDisplay->setString(*_username);
	window->requestFocus();
}

void Lobby::launchHearingThread()
{
	std::thread hearing(&Lobby::hearingThread, this, &socket, &lobbyIsRunning, &criticalOn, &gameID, &allGood, &scene);
	hearing.detach();
}

void Lobby::hearingThread(sf::UdpSocket * _socket, bool * _lobbyIsRunning, bool* _criticalOn, int* _gameId, bool* _allGood, int*_scene)
{
	while (*_lobbyIsRunning) {
		sf::Packet hearedPacket;
		sf::IpAddress hearedIp;
		unsigned short hearedShort;
		_socket->receive(hearedPacket, hearedIp, hearedShort);
		if (hearedIp == bg::serverIP && hearedShort == bg::serverPort) {
			bg::S2CProt code;
			hearedPacket >> code;
			switch (code) {
				case bg::S2CProt::ACK_S2C:
					*_criticalOn = false;
				break;
				case bg::S2CProt::LOBBY_GO: {
					*_criticalOn = false;
					int newID = -1;
					hearedPacket >> newID;
					*_gameId = newID;
					std::cout << "id: " << *_gameId << std::endl;
					*_allGood = true;
				}
					break;
				case bg::S2CProt::SIGNUP_LOGIN_DONE:
					std::cout << "All good" << std::endl;
					*_scene = 1;
					break;
				case bg::S2CProt::SIGNUP_LOGIN_ERROR:
					std::cout << "Either username or password are not valid" << std::endl;
					launchAuthenticathionThread();
					break;
			}
		}
	}
}

void Lobby::launchCriticalThread(sf::Packet  _criticalPacket)
{
	std::thread critical(&Lobby::criticalThread, this, _criticalPacket, &criticalOn, &socket);
	critical.detach();
}

void Lobby::criticalThread(sf::Packet  _criticalPacket, bool* _criticalOn, sf::UdpSocket *_socket)
{
	sf::Clock criticalClock;
	while (*_criticalOn) {
		if (criticalClock.getElapsedTime().asSeconds() >= bg::CriticalWait) {

			_socket->send(_criticalPacket, bg::serverIP, bg::serverPort);
			criticalClock.restart();
		}
	}
}
