#include "NetworkManagerClient.h"
#include "Lobby.h"
#include <iostream>

#define RUN_LOBBY

void main() {
	bool close = false;
	int scene = 0;
	std::string username = "def";

#ifdef RUN_LOBBY
	redoLogin:
	while(1){	
		Lobby lobby(scene, username);
		lobby.close = &close;
		lobby.mainLoop();

		if (!close) {
			NetworkManagerClient manager(lobby.gameID, lobby.username);
			manager.MainLoop();
		}
	}
#endif
#ifndef RUN_LOBBY
	if (!close) {
		NetworkManagerClient manager(0, "Maxillermo");
		manager.MainLoop();
	}
#endif
}