#pragma once
#include <Definition.h>

#include <SFML\Network.hpp>
#include <iostream>
#include <map>
#include <queue>
#include <thread>
#include <mutex>
#include <vector>
#include <deque>

class NetworkManagerServer
{
public:
	NetworkManagerServer();
	~NetworkManagerServer();
	
	bool serverOn = true;

	//ServerConnexion
	sf::UdpSocket socket;

	//Players
	std::map<int, bg::Player> players;
	int getValidID(std::map<int, bg::Player>*_players);
	bg::MovementProcessor moveProcessor;
	std::map<int, bg::Game> games;

	//Lobby clients
	std::vector<bg::WaitingClient> waitingClients;

	//MainLoop
	void MainLoop();

	//CriticalThread
	void LaunchCriticalThread();
	void CriticalThread(std::queue<bg::CritPacket_S2C> *_criticalQ, sf::UdpSocket* _socket, bool *_serverOn);
	std::queue<bg::CritPacket_S2C> criticalQ;

	//HearingThread
	void LaunchHearingThread();
	void HearingThread(sf::UdpSocket* _socket, std::map<int, bg::Player>* _players, std::queue<bg::CritPacket_S2C>* _criticalQ, bool *_serverOn, 
		bg::MovementProcessor* _moveProcessor, std::vector<bg::WaitingClient> *_waitingClients, std::map<int, bg::Game> *_games);

	//HeartbeatThread
	void LaunchHeartbeatThread();
	void HeartbeatThread(sf::UdpSocket *_socket, std::map<int, bg::Player> *_players, bool*_serverOn);

	//EnemyGenerationThread
	void LaunchEnemyGenThread();
	void EnemyGenThread(std::map<int, bg::Game>* _games, bool *_serverOn);

};

