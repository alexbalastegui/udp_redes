#include "NetworkManagerServer.h"

std::mutex clientMapMutex;
std::mutex powerUpEnemyMutex;

NetworkManagerServer::NetworkManagerServer()
{
	socket.bind(bg::serverPort, bg::serverIP);

	LaunchHearingThread();
	LaunchHeartbeatThread();
	LaunchEnemyGenThread();

	std::cout << "Port: " << socket.getLocalPort() << std::endl;

	//DATABASE
	//carga los valores de la base de datos para los powerups y los pones en estas variables. Tienen que valer lo que ya he puesto o algo parecido. Los que suben algo por encima de 1, y los que lo bajan por debajo de 1.
	/*bg::powerUpSizeDownVal = 0.85f;
	bg::powerUpSizeUpVal = 1.15f;
	bg::powerUpSpeedUpVal = 1.15f;
	bg::powerUpSpeedDownVal = 0.85f;*/
}


NetworkManagerServer::~NetworkManagerServer()
{
}

int NetworkManagerServer::getValidID(std::map<int, bg::Player>*_players)
{
	for (int i = 0; i < INT16_MAX; ++i) {
		if (_players->find(i) == _players->end()) {//No existe este �ndice
			return i;
		}
	}
	return 0;
}

void NetworkManagerServer::MainLoop()
{
	while (serverOn) {
		moveProcessor.processOrder(&socket, &players, &games, &clientMapMutex);

		sf::Clock gameClock;
		while (serverOn) {
			moveProcessor.processOrder(&socket, &players, &games, &clientMapMutex);


			if (gameClock.getElapsedTime().asSeconds() > bg::ServerGameLogicUpdateTime) {

				for (auto it = games.begin(); it != games.end(); ++it) {//Itera por todas las partidas que hay

					if (!it->second.gameFinished) {

						if (it->second.one != nullptr && it->second.two != nullptr) {
							powerUpEnemyMutex.lock();
							for (auto it2 = it->second.enemies.begin(); it2 != it->second.enemies.end();) { //Itera por los enemigos de la partida para actualizarlos o eliminarlos si se salen de los l�mites del mundo
								(*it2)->Update(&(it->second));
								if ((*it2)->getPos().y>bg::windowY) {
									it2=it->second.enemies.erase(it2);
								}
								else {
									++it2;
								}
							}
							for (auto it2 = it->second.powerups.begin(); it2 != it->second.powerups.end();) { //itera por los powerups para actualizarlos o eliminarlos si se salen de los l�mites del mundo
								(*it2)->Update();
								if ( ((*it2)->getPos().y > bg::windowY) || 
									(bg::almostEqualVec((*it2)->getPos(), it->second.one->getPosition(), bg::enemyRad + (bg::playerRad*it->second.one->sizeScalar))) ||
									(bg::almostEqualVec((*it2)->getPos(), it->second.two->getPosition(), bg::enemyRad + (bg::playerRad*it->second.two->sizeScalar))) ) {
																		
									if (bg::almostEqualVec((*it2)->getPos(), it->second.one->getPosition(), bg::enemyRad + (bg::playerRad*it->second.one->sizeScalar))) {
										sf::Packet powerUpPack;
										powerUpPack << bg::S2CProt::POWERUP;
										switch ((*it2)->type) {
										case bg::PowerUpType::SizeDown:
											it->second.one->setSize(it->second.one->sizeScalar * bg::powerUpSizeDownVal);
											powerUpPack << bg::PowerUpType::SizeDown << it->second.one->getPlayerID() << (bg::powerUpSizeDownVal);
											break;
										case bg::PowerUpType::SizeUp:
											it->second.two->setSize(it->second.two->sizeScalar * bg::powerUpSizeUpVal);
											powerUpPack << bg::PowerUpType::SizeDown << it->second.two->getPlayerID() << (bg::powerUpSizeUpVal);
											break;
										case bg::PowerUpType::SpeedDown:
											it->second.two->setSpeed(it->second.two->speedScalar*bg::powerUpSpeedDownVal);
											powerUpPack << bg::PowerUpType::SpeedDown << it->second.two->getPlayerID() << (bg::powerUpSpeedDownVal);
											break;
										case bg::PowerUpType::SpeedUp:
											it->second.one->setSpeed(it->second.one->speedScalar*bg::powerUpSpeedUpVal);
											powerUpPack << bg::PowerUpType::SpeedUp << it->second.one->getPlayerID() << (bg::powerUpSpeedUpVal);
											break;
										}
										socket.send(powerUpPack, it->second.one->ipAddress, it->second.one->port);
										socket.send(powerUpPack, it->second.two->ipAddress, it->second.two->port);
									}
									else if (bg::almostEqualVec((*it2)->getPos(), it->second.two->getPosition(), bg::enemyRad + (bg::playerRad*it->second.two->sizeScalar))) {
										sf::Packet powerUpPack;
										powerUpPack << bg::S2CProt::POWERUP;
										switch ((*it2)->type) {
										case bg::PowerUpType::SizeDown:
											it->second.two->setSize(it->second.two->sizeScalar * bg::powerUpSizeDownVal);
											powerUpPack << bg::PowerUpType::SizeDown << it->second.two->getPlayerID() << (bg::powerUpSizeDownVal);
											break;
										case bg::PowerUpType::SizeUp:
											it->second.one->setSize(it->second.one->sizeScalar * bg::powerUpSizeUpVal);
											powerUpPack << bg::PowerUpType::SizeDown << it->second.one->getPlayerID() << (bg::powerUpSizeUpVal);
											break;
										case bg::PowerUpType::SpeedDown:
											it->second.one->setSpeed(it->second.one->speedScalar*bg::powerUpSpeedDownVal);
											powerUpPack << bg::PowerUpType::SpeedDown << it->second.one->getPlayerID() << (bg::powerUpSpeedDownVal);
											break;
										case bg::PowerUpType::SpeedUp:
											it->second.two->setSpeed(it->second.two->speedScalar*bg::powerUpSpeedUpVal);
											powerUpPack << bg::PowerUpType::SpeedUp << it->second.two->getPlayerID() << (bg::powerUpSpeedUpVal);
											break;
										}
										socket.send(powerUpPack, it->second.one->ipAddress, it->second.one->port);
										socket.send(powerUpPack, it->second.two->ipAddress, it->second.two->port);
									}

									it2 = it->second.powerups.erase(it2);
								}
								else {
									++it2;
								}
							}
							sf::Packet enemyPackO;
							enemyPackO << bg::S2CProt::ENEMIES << static_cast<int>(it->second.enemies.size());

							for (auto it2 = it->second.enemies.begin(); it2 != it->second.enemies.end(); ++it2) { //Itera por los enemigos de la partida para meterlos en el packet
								enemyPackO << *(*it2);
							}
							enemyPackO << static_cast<int>(it->second.powerups.size());
							for (auto it2 = it->second.powerups.begin(); it2 != it->second.powerups.end(); ++it2) { //Itera por los enemigos de la partida para meterlos en el packet
								enemyPackO << *(*it2);
							}

							it->second.gameFinished = !it->second.one->isAlive || !it->second.two->isAlive; //La partida ha acabado si uno de los dos jugadores muere

							enemyPackO << it->second.gameFinished;

							if (!it->second.one->isAlive) {
								enemyPackO << it->second.one->getPlayerID();
							}
							else if (!it->second.two->isAlive) {
								enemyPackO << it->second.two->getPlayerID();
							}

							socket.send(enemyPackO, it->second.one->ipAddress, it->second.one->port); //debe ser cr�tico si el gamefinished es true
							socket.send(enemyPackO, it->second.two->ipAddress, it->second.two->port);
							powerUpEnemyMutex.unlock();
						}
					}
				}
				gameClock.restart();
			}
		}
	}
}

void NetworkManagerServer::LaunchCriticalThread()
{
	std::thread criticalThread(&NetworkManagerServer::CriticalThread, this, &criticalQ, &socket, &serverOn);
	criticalThread.detach();
}

void NetworkManagerServer::CriticalThread(std::queue<bg::CritPacket_S2C>* _criticalQ, sf::UdpSocket * _socket, bool * _serverOn)
{
	sf::Clock criticalClock;
	while (*_serverOn) {
		while (!_criticalQ->empty() && *_serverOn) {
			if (criticalClock.getElapsedTime().asSeconds() >= bg::CriticalWait) {
				_socket->send(_criticalQ->front().packet, bg::serverIP, bg::serverPort);
				criticalClock.restart();
			}
		}
		criticalClock.restart();
	}
}

void NetworkManagerServer::LaunchHearingThread()
{
	std::thread criticalThread(&NetworkManagerServer::HearingThread, this, &socket, &players, &criticalQ, &serverOn, &moveProcessor, &waitingClients, &games);
	criticalThread.detach();
}

void NetworkManagerServer::HearingThread(sf::UdpSocket * _socket, std::map<int, bg::Player>* _players, std::queue<bg::CritPacket_S2C>* _criticalQ, bool *_serverOn, 
	bg::MovementProcessor* _moveProcessor, std::vector<bg::WaitingClient> *_waitingClients, std::map<int, bg::Game> *_games)
{
	sf::Packet hearedPacket;
	unsigned short hearedPort;
	sf::IpAddress hearedIp;
	while (*_serverOn) {
		auto status = _socket->receive(hearedPacket, hearedIp, hearedPort);
		if (status == sf::Socket::Status::Done) {
			bg::C2SProt code;
			int playerID;
			hearedPacket >> code >> playerID;

			//if (_players->find(playerID) != _players->end()) { //Arregla 
			//	if ((*_players)[playerID].ipAddress != hearedIp || (*_players)[playerID].port != hearedPort) {
			//		(*_players)[playerID].ipAddress = hearedIp;
			//		(*_players)[playerID].port = hearedPort;
			//	}
			//}

			switch (code) {
			case bg::C2SProt::HELLO: {
				if (_games->find(playerID) == _games->end() || 1){
					std::cout << hearedIp.toString() << " : " << hearedPort << " - wants to connect" << std::endl;

					bool alreadyExisting = false;
					for (auto it = _players->begin(); it != _players->end(); ++it) { //Itera players y comprueba si hay alg�n player guardado que coincida con el que pide conectarse
						if (it->second.port == hearedPort && it->second.ipAddress == hearedIp) {
							alreadyExisting = true;
							std::cout << hearedPort <<": trying to connect already exists" << std::endl;
							break;
						}
					}

					if (!alreadyExisting) {
						std::cout << "Generando player para: " << hearedPort << std::endl;
						int newPlayerID = getValidID(_players); //Generamos un nuevo player
						(*_players)[newPlayerID].setPlayerID(newPlayerID);
						(*_players)[newPlayerID].port = hearedPort;
						(*_players)[newPlayerID].ipAddress = hearedIp;
						(*_players)[newPlayerID].setRandomPosition();
						(*_players)[newPlayerID].gameID = playerID;

						if ((*_games)[playerID].one == nullptr) {
							(*_players)[newPlayerID].setPosition({ (*_players)[newPlayerID].getPosition().x,bg::player1Y });
							(*_games)[playerID].one = &(*_players)[newPlayerID];
							std::cout << "First slot assigned to : "<< hearedPort << std::endl;

							//Se env�a al propio player
							sf::Packet welcomePacket;
							welcomePacket << bg::S2CProt::WELCOME << *(*_games)[playerID].one; 
							_socket->send(welcomePacket, hearedIp, hearedPort);

						}
						else {
							(*_players)[newPlayerID].setPosition({ (*_players)[newPlayerID].getPosition().x,bg::player2Y });
							(*_games)[playerID].two = &(*_players)[newPlayerID];
							std::cout << "Second slot assigned to : " << hearedPort << std::endl;

							//Se env�a el player generado al mismo cliente y al cliente ya existente
							sf::Packet welcomePacket;
							welcomePacket << bg::S2CProt::WELCOME << *(*_games)[playerID].two;
							_socket->send(welcomePacket, hearedIp, hearedPort);
							sf::Packet newPTwo;
							newPTwo << bg::S2CProt::NEWPLAYER << *(*_games)[playerID].two;
							_socket->send(newPTwo, (*_games)[playerID].one->ipAddress, (*_games)[playerID].one->port);

							sf::Packet newPOne;
							newPOne << bg::S2CProt::NEWPLAYER << *(*_games)[playerID].one;
							_socket->send(newPOne, hearedIp, hearedPort);
						}						
					}
				}
			}
				break;
			case bg::C2SProt::MOVE: {
				int gameID = -1;
				bg::Movement requestedMovement;
				hearedPacket >> gameID >> requestedMovement;
				_moveProcessor->addOrder(requestedMovement, playerID, hearedIp, hearedPort, gameID); //falta a�adir el gameid desde el cliente cuando este se mueve
			}
				break;
			case bg::C2SProt::PONG: {
				clientMapMutex.lock();
				(*_players)[playerID].isConfirmed = 0;
				clientMapMutex.unlock();
			}
				break;
			case bg::C2SProt::DISCONNECT: {
				sf::Packet confirmDisconnect;
				confirmDisconnect << bg::S2CProt::OKDISCONNECT;
				_socket->send(confirmDisconnect, (*_players)[playerID].ipAddress, (*_players)[playerID].port);

				_players->erase(playerID);
				std::cout << "Wants to disconnect" << std::endl;

				sf::Packet disconnectedPlayer;
				disconnectedPlayer << bg::S2CProt::PLAYERDISCONNECTED << playerID;
				for (auto it = _players->begin(); it != _players->end(); ++it) {
					_socket->send(disconnectedPlayer, it->second.ipAddress, it->second.port);
				}
			}
				break;
			case bg::C2SProt::ACK_C2S:
				if (_criticalQ->front().ackCode == bg::C2SProt::ACK_C2S && !_criticalQ->empty())
					_criticalQ->pop();
				break;
			case bg::C2SProt::LOBBY_READY: {
				_waitingClients->push_back({hearedIp, hearedPort, 0});

				if (_waitingClients->size() >= 2) {
					static int gamesIndex = 0;
					for (auto it = _waitingClients->begin(); it != _waitingClients->end(); ++it) { //Fix with critical
						sf::Packet lobbyAns;
						it->gameID = gamesIndex;
						lobbyAns << bg::S2CProt::LOBBY_GO << it->gameID;
						_socket->send(lobbyAns, it->ip, it->port);
						lobbyAns.clear();
					}
					_waitingClients->clear();
					gamesIndex++;
				}
				else {
					sf::Packet lobbyAns;
					lobbyAns << bg::S2CProt::ACK_S2C;
					_socket->send(lobbyAns, hearedIp, hearedPort);
				}
			}
				break;
			case bg::C2SProt::LOGIN: {
				std::string username, password;
				hearedPacket >> username >> password;

				//DATABASE
				//Comprobar el username y la password contra la base de datos

				if (1/*Los datos son v�lidos*/) {
					sf::Packet pack;
					pack << bg::S2CProt::SIGNUP_LOGIN_DONE;
					_socket->send(pack, hearedIp, hearedPort);
				}
				else {
					sf::Packet pack;
					pack << bg::S2CProt::SIGNUP_LOGIN_ERROR;
					_socket->send(pack, hearedIp, hearedPort);
				}

			}
				break;
			case bg::C2SProt::SIGNUP: {
				std::string username, password;
				hearedPacket >> username >> password;

				//Comprobar el username y la password contra la base de datos


				if (1/*Los datos son v�lidos*/) {
					sf::Packet pack;
					pack << bg::S2CProt::SIGNUP_LOGIN_DONE;
					_socket->send(pack, hearedIp, hearedPort);
				}
				else {
					sf::Packet pack;
					pack << bg::S2CProt::SIGNUP_LOGIN_ERROR;
					_socket->send(pack, hearedIp, hearedPort);
				}
			}
				break;
			}
		}
	}
}

void NetworkManagerServer::LaunchHeartbeatThread()
{
	std::thread heartbeat(&NetworkManagerServer::HeartbeatThread, this, &socket, &players, &serverOn);
	heartbeat.detach();
}

void NetworkManagerServer::HeartbeatThread(sf::UdpSocket * _socket, std::map<int, bg::Player>* _players, bool*_serverOn)
{
	sf::Clock pingClock;
	while (*_serverOn) {
		if (pingClock.getElapsedTime().asSeconds() > bg::ServerPingTime) {
			sf::Packet pingPacket;
			pingPacket << bg::S2CProt::PING;
			clientMapMutex.lock();
			for (auto it = _players->begin(); it != _players->end();) {
				if (it->second.isConfirmed<bg::PingFailsToDisconnect) {
					_socket->send(pingPacket, it->second.ipAddress, it->second.port);
					it->second.isConfirmed++;
					++it;
				}
				else {
					_players->erase(it++);
				}
			}
			clientMapMutex.unlock();
			pingClock.restart();
		}
	}
}

void NetworkManagerServer::LaunchEnemyGenThread()
{
	std::thread EnemyGen(&NetworkManagerServer::EnemyGenThread, this, &games, &serverOn);
	EnemyGen.detach();
}

void NetworkManagerServer::EnemyGenThread(std::map<int, bg::Game>* _games, bool * _serverOn)
{
	sf::Clock enemyGenClock;
	while (*_serverOn) {
		if (enemyGenClock.getElapsedTime().asSeconds() > bg::ServerEnemyGenerationTime) {
			powerUpEnemyMutex.lock();
			for (auto it = _games->begin(); it != _games->end(); ++it) {
				//Enemy generation
				if (!it->second.gameFinished) {
					it->second.enemies.push_back(new bg::Enemy);

					//Powerup generation
					int spawnPowerUp = rand() % 100;
					if (spawnPowerUp < bg::powerUpSpawnRate) {
						it->second.powerups.push_back(new bg::PowerUp(static_cast<bg::PowerUpType>(rand()%static_cast<int>(bg::PowerUpType::MAX))));
					}
				}
			}
			enemyGenClock.restart();
			powerUpEnemyMutex.unlock();
		}
	}
}
