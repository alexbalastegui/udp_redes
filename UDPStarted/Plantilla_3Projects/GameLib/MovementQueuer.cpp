#include "Definition.h"



bg::MovementQueuer::MovementQueuer()
{
}


bg::MovementQueuer::~MovementQueuer()
{
}

void bg::MovementQueuer::addOrder(bg::Movement _mov)
{
	if (inputQ.empty()) {
		_mov.id = messageID;
		inputQ.push(_mov); //Es necesario mezclar los mensajes para enviarle una sola orden al servidor
	}
	else {
		inputQ.front().delta.x += _mov.delta.x;
		inputQ.front().delta.y += _mov.delta.y;
		inputQ.front().abs.x = _mov.abs.x;
		inputQ.front().abs.y = _mov.abs.y;

	}
}

void bg::MovementQueuer::processOrder(sf::UdpSocket* _socket, int _playerID)
{
	if (processClock.getElapsedTime().asSeconds() > bg::MovementEnqueuerProcessTime && !inputQ.empty()) {

		sf::Packet movementRequest;
		movementRequest << bg::C2SProt::MOVE << _playerID << gameID << inputQ.front(); //Send the movement in the inputQ
		_socket->send(movementRequest, bg::serverIP, bg::serverPort);

		movementHistory.push_back(inputQ.front()); //Add the sent movement to the historyQ for reconciliation
		//std::cout << "Send: " << inputQ.front().id << ": " << inputQ.front().abs.x << " - " << inputQ.front().abs.y << std::endl;
		inputQ.pop();
		messageID++;
		processClock.restart();
	}
}

void bg::MovementQueuer::acknowledgeOrder(bg::Movement _acknowledgedMov, bg::Player* _player)
{
		//Check if positions match and then operate consequently
	bool reconciliate = false;
	sf::Vector2f newPos = {0, 0};
		auto it = movementHistory.begin();
		for (; it != movementHistory.end(); ++it) {
			if (_acknowledgedMov.id == it->id) {
				reconciliate = !(bg::almostEqualVec(_acknowledgedMov.abs, it->abs, bg::ReconciliationTolerance));
				if (reconciliate) {
					newPos = it->abs;
					std::cout << "Reconciliation!" << std::endl;
					std::cout << "Order pos: " << _acknowledgedMov.id << ": " << _acknowledgedMov.abs.x << " - " << _acknowledgedMov.abs.y << std::endl;
					std::cout << "Player pos: " << it->id << ": " << it->abs.x << " - " << it->abs.y << std::endl;
				}
				break;
			}
		}

		if (reconciliate) {
			_player->setPosition(newPos);
			movementHistory.erase(it, movementHistory.end());
		}
		else {
			movementHistory.erase(movementHistory.begin(), it);
		}

}
