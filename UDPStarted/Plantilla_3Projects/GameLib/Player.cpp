#include "Definition.h"



bg::Player::Player()
{
	ipAddress = sf::IpAddress("127.0.0.1");
	port = 55000;
	playerID = -1;

	drawable = sf::CircleShape(playerRad*sizeScalar);
}

bg::Player::~Player()
{
}

sf::Vector2f bg::Player::getPosition() {
	return pos;
}

sf::Vector2f bg::Player::getPosition() const {
	return pos;
}

sf::Vector2f bg::Player::getFollowPosition() const
{
	return followPos;
}

void bg::Player::setPosition(sf::Vector2f _pos) {
	pos = _pos;
	drawable.setPosition(_pos);
}

void bg::Player::setSize(float _newSize) {
	sizeScalar = _newSize;
	drawable.setRadius(bg::playerRad*sizeScalar);
}

void bg::Player::setSpeed(float _newSpeed) {
	speedScalar = _newSpeed;
}

void bg::Player::setPositionLerp(float alpha)
{
	setPosition({ lerp(pos.x, followPos.x, alpha),lerp(pos.y, followPos.y, alpha) });
}

void bg::Player::setFollowablePos(sf::Vector2f _pos)
{
	followPos = _pos;
}

void bg::Player::setRandomPosition() {
	pos.x = static_cast<float>(rand() % windowX);
	pos.y = static_cast<float>(rand() % windowY);
	drawable.setPosition(pos);
	followPos = pos;
}

int bg::Player::getPlayerID() {
	return playerID;
}

int bg::Player::getPlayerID() const {
	return playerID;
}

void bg::Player::setPlayerID(int _id) {
	playerID = _id;
	drawable.setFillColor(idToColor());
}

sf::Color bg::Player::idToColor() {
	switch (playerID) {
	case 0:
		return sf::Color::Green;
		break;
	case 1:
		return sf::Color::Red;
		break;
	case 2:
		return sf::Color::Blue;
		break;
	case 3:
		return sf::Color::Yellow;
		break;
	case 4:
		return sf::Color::Magenta;
		break;
	case 5:
		return sf::Color::Cyan;
		break;
	case 6:
		return sf::Color::Black;
		break;
	case 7:
		return sf::Color::White;
		break;
	}
	return sf::Color::White;
}

void bg::Player::move(bg::dir _direction, float _deltaTime) {
	switch (_direction) {
	case bg::dir::_left:
		setPosition({ getPosition().x - bg::playerVel*speedScalar*_deltaTime ,getPosition().y });
		inputQueuer->addOrder({ -1, {-playerVel * speedScalar* _deltaTime, 0}, {getPosition().x, getPosition().y} });
		break;
	case bg::dir::_right:
		setPosition({ getPosition().x + bg::playerVel*speedScalar*_deltaTime ,getPosition().y });
		inputQueuer->addOrder({ -1,{ playerVel*speedScalar*_deltaTime, 0 },{ getPosition().x, getPosition().y } });
		break;
	case bg::dir::_up:
		setPosition({ getPosition().x  ,getPosition().y - bg::playerVel*speedScalar*_deltaTime });
		inputQueuer->addOrder({ -1,{ 0, -playerVel * speedScalar* _deltaTime },{ getPosition().x, getPosition().y } });
		break;
	case bg::dir::_down:
		setPosition({ getPosition().x  ,getPosition().y + bg::playerVel*speedScalar*_deltaTime });
		inputQueuer->addOrder({ -1,{ 0, playerVel*speedScalar*_deltaTime },{ getPosition().x, getPosition().y } });
		break;
	}
	//El id del mensaje lo gestiona el MovementQueuer
	//messageID++;
}

void bg::Player::Draw(sf::RenderWindow* _window) {
	_window->draw(drawable);
}

void bg::Player::Update(float _deltaTime, sf::UdpSocket* _socket) {
	if (inputQueuer != nullptr) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
			move(dir::_left, _deltaTime);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
			move(dir::_right, _deltaTime);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
			move(dir::_up, _deltaTime);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
			move(dir::_down, _deltaTime);
		}

		inputQueuer->processOrder(_socket, playerID);
	}
}

bg::Player & bg::Player::operator=(const Player & rhs)
{
	ipAddress = rhs.ipAddress;
	port = rhs.port;
	setPlayerID(rhs.getPlayerID());
	setPosition(rhs.getPosition());
	setFollowablePos(rhs.getFollowPosition());

	return *this;
}

sf::Packet & operator<<(sf::Packet & packet, const bg::Player & player)
{
	return packet << player.ipAddress.toInteger() << player.port << player.getPlayerID() << player.getPosition().x << player.getPosition().y << player.gameID << player.sizeScalar;// << player.getFollowPosition().x << player.getFollowPosition().y;
}

sf::Packet & operator>>(sf::Packet & packet, bg::Player & player)
{
	int localIP, localID;
	float posX, posY;
	packet >> localIP >> player.port >> localID >> posX >> posY >> player.gameID;// >> fPosX >> fPosY;
	player.ipAddress = sf::IpAddress(localIP);
	player.setPlayerID(localID);
	player.setPosition({posX, posY});
	//player.setFollowablePos({ fPosX ,fPosY });

	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const bg::WaitingClient & client)
{
	return packet << client.ip.toInteger() << client.port << client.gameID;
}

sf::Packet & operator>>(sf::Packet & packet, bg::WaitingClient & client)
{
	int ip;
	packet >> ip >> client.port >> client.gameID;
	client.ip = sf::IpAddress(ip);
	return packet;
}
