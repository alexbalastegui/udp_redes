#include "Definition.h"



bg::MovementProcessor::MovementProcessor()
{
}


bg::MovementProcessor::~MovementProcessor()
{
}

void bg::MovementProcessor::addOrder(bg::Movement _mov, int _playerID, sf::IpAddress _ip, unsigned short _port, int _gameID)
{
	bool add = true;
	for (auto it = clientsMoves.begin(); it != clientsMoves.end(); ++it) {
		if (it->playerID == _playerID) {
			//comprobar si el player que est� a�adiendo una orden ya tiene una alojada en la deq. Si es as� combinar las ordenes. si no, a�adir la orden como una nueva

			it->mov.delta.x += _mov.delta.x;
			it->mov.delta.y += _mov.delta.y;
			it->mov.abs.x = _mov.abs.x;
			it->mov.abs.y = _mov.abs.y;
			it->mov.id = _mov.id;

			//stack
			add = false;
			break;
		}
	}

	if (add) {
		clientsMoves.push_back({_playerID, _ip, _port, _mov, _gameID});
	}

}

void bg::MovementProcessor::processOrder(sf::UdpSocket* _socket, std::map<int, bg::Player> *_players, std::map<int, bg::Game> *_games, std::mutex* _mutex)
{
	if (processClock.getElapsedTime().asSeconds() > bg::ServerProcessMovementTime && !clientsMoves.empty()) {
		_mutex->lock();
		for (auto it = clientsMoves.begin(); it != clientsMoves.end(); ++it) {
			sf::Packet pack;
			pack << bg::S2CProt::OKMOVE << it->playerID << it->mov;
			(*_players)[clientsMoves.front().playerID].setPosition(it->mov.abs);

			_socket->send(pack, (*_games)[clientsMoves.front().gameID].one->ipAddress, (*_games)[clientsMoves.front().gameID].one->port);
			_socket->send(pack, (*_games)[clientsMoves.front().gameID].two->ipAddress, (*_games)[clientsMoves.front().gameID].two->port);
			
			//std::cout << "Sending movement to " << it->port << " - " << it->addressIp << std::endl;
		}

		clientsMoves.clear();
		processClock.restart();
		_mutex->unlock();
	}
}
