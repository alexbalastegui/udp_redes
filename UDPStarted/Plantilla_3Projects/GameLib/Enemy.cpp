#include "Definition.h"


bg::Enemy::Enemy()
{
	drawable = sf::CircleShape(bg::enemyRad, 3);
	drawable.setFillColor(bg::enemyCol);
	setPos({static_cast<float>(rand()%bg::windowX), 10.f});
}

bg::Enemy::~Enemy()
{
}

sf::Vector2f bg::Enemy::getPos()
{
	return pos;
}

sf::Vector2f bg::Enemy::getPos() const
{
	return pos;
}

void bg::Enemy::setPos(sf::Vector2f _newPos)
{
	pos = _newPos;
	drawable.setPosition(pos);
}

void bg::Enemy::Update(bg::Game * _room)
{
	setPos({ pos.x, pos.y + vel });

	if (_room->one != nullptr && _room->two != nullptr) {
		if (_room->one->isAlive && _room->two->isAlive) {
			if (bg::almostEqualVec(getPos(), _room->one->getPosition(), bg::enemyRad+bg::playerRad)) {
				//choco con el player One
				_room->one->isAlive = false;
			}
			else if (bg::almostEqualVec(getPos(), _room->two->getPosition(), bg::enemyRad + bg::playerRad)) {
				//choco con el player two
				_room->two->isAlive = false;
			}
		}
	}
}

void bg::Enemy::Draw(sf::RenderWindow * _window)
{
	_window->draw(drawable);
}

sf::Packet & operator<<(sf::Packet & packet, const bg::Enemy & enemy)
{
	return packet << enemy.getPos().x << enemy.getPos().y;
}

sf::Packet & operator>>(sf::Packet & packet, bg::Enemy & enemy)
{
	float posX, posY;
	packet >> posX >> posY;
	enemy.setPos({ posX, posY });
	return packet;
}