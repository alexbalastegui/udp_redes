#include "Definition.h"

sf::Packet & operator<<(sf::Packet & packet, const bg::C2SProt & prot)
{
	return packet << static_cast<int>(prot);
}

sf::Packet & operator>>(sf::Packet & packet, bg::C2SProt & prot)
{
	int aux;
	packet >> aux;
	prot = static_cast<bg::C2SProt>(aux);
	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const bg::S2CProt & prot)
{
	return packet << static_cast<int>(prot);
}

sf::Packet & operator>>(sf::Packet & packet, bg::S2CProt & prot)
{
	int aux;
	packet >> aux;
	prot = static_cast<bg::S2CProt>(aux);
	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const bg::Movement & mov)
{
	return packet << mov.id << mov.delta.x << mov.delta.y << mov.abs.x << mov.abs.y;
}

sf::Packet & operator>>(sf::Packet & packet, bg::Movement & mov)
{
	return packet >> mov.id >> mov.delta.x >> mov.delta.y >> mov.abs.x >> mov.abs.y;
}

float bg::randomFloat()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> dis(0.f, 1.f);
	return dis(gen);
}

bool bg::almostEqualVec(sf::Vector2f &a, sf::Vector2f &b, float tolerance)
{
	return ((a.x >= b.x - tolerance && a.x <= b.x + tolerance) && (a.y >= b.y - tolerance && a.y <= b.y + tolerance));
}

float bg::lerp(float a, float b, float f)
{
	return a + f * (b - a);
}
