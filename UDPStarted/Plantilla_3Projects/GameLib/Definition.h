#pragma once
#include <SFML\Network.hpp>
#include <SFML\Graphics.hpp>
#include <string>
#include <random>
#include <queue>
#include <deque>
#include <map>
#include <iostream>
#include <mutex>

#define BUILD

namespace bg {
	//Protocol
	enum class C2SProt
	{
		HELLO, //El jugador pide establecer conexi�n con el server
		MOVE, //El jugador env�a una petici�n de movimiento 
		PONG, //El jugador informa de que ha recibido el PING
		DISCONNECT, //El jugador env�a una petici�n de desconexi�n
		ACK_C2S, //Acknowledge para mensajes cr�ticos de cliente a servidor
		SIGNUP, //El cliente pide hacer sign up contra la base de datos
		LOGIN, //El cliente pide hacer log in contra la base de datos

		LOBBY_READY //Indica que el jugador está listo para empezar una partida
	};
	enum class S2CProt 
	{
		WELCOME, //El server confirma la conexi�n del nuevo jugador
		NEWPLAYER, //El server informa del nuevo jugador a los jugadores anteriores
		OKMOVE, //El server confirma la petici�n de movimiento del cliente
		PING, //Env�a una se�al para confirmar la conexi�n
		PLAYERDISCONNECTED, //Informa a los jugadores restantes de la desconexi�n de cierto jugador
		OKDISCONNECT, //Confirma la petici�n de desconexi�n
		ACK_S2C, //Acknowledge para mensajes cr�ticos de servidor a cliente
		ENEMIES, //Envía la nueva posición de los enemigos y los powerups
		SIGNUP_LOGIN_DONE, //Da la señal de que el player ya ha iniciado sesión y que ya puede empezar a jugar
		SIGNUP_LOGIN_ERROR, //Da la señal de que ha habido un error al iniciar la sesión
		POWERUP, //Indica a los clientes que alguien ha recogido un powerup y que tipo de cambio debe aplicarsele

		LOBBY_GO //Da la señal para que la partida inicie
	};

//Structs
	//criticalPackets
	struct CritPacket_S2C {
		sf::Packet packet;
		C2SProt ackCode;
		unsigned short port;
		sf::IpAddress ipAddress;
	};
	struct CritPacket_C2S {
		sf::Packet packet;
		S2CProt ackCode;
	};
	//movement direction
	enum class dir {
		_left, _right, _up, _down, _MAX
	};
	struct Movement { //holds the movement information of where the player is requesting to go when sent to server, and the information of where the server orders the player to go when sent to client
		int id;
		sf::Vector2f delta;
		sf::Vector2f abs;
	};
	struct Movement_Server {
		int playerID;
		sf::IpAddress addressIp;
		unsigned short port;
		Movement mov;
		int gameID;
	};
	struct WaitingClient {
		sf::IpAddress ip;
		unsigned short port;
		int gameID;
	};
	class Player;
	class Enemy;
	class PowerUp;
	struct Game {
		Player* one = nullptr;
		Player* two = nullptr;
		int gameID;
		std::deque<bg::Enemy*> enemies;
		std::deque<bg::PowerUp*> powerups;
		bool gameFinished = false;
	};
	enum class PowerUpType {
		SpeedUp, 
		SpeedDown, 
		SizeUp, 
		SizeDown,
		MAX
	};


//Data loss in client reception
	float randomFloat();

//Methods
	bool almostEqualVec(sf::Vector2f &a, sf::Vector2f &b, float tolerance);
	float lerp(float a, float b, float f);

//Constants
	//server
	const sf::IpAddress serverIP = sf::IpAddress("127.0.0.1");
	const unsigned short serverPort = 50002;
	const int PingFailsToDisconnect = 10;

	//timeAmounts
	const float CriticalWait = 3.0f;
	const float MovementEnqueuerProcessTime = 0.2f;
	const float ServerProcessMovementTime = 0.3f;
	const float ServerPingTime = 5.f;
	const float ServerEnemyGenerationTime = 5.f; 
	const float ServerGameLogicUpdateTime = 0.8f;

	//gameValues
	const int windowX = 400;
	const int windowY = 300;
	const std::string gameName = "Ball Game";
	const float playerRad = 15;
	const float playerVel = 240;
	const sf::Color bgColor = sf::Color(211, 132, 93, 255);
	const float PERCENT_PACKETLOSS = 0.f;
	const float ReconciliationTolerance = 3.f;
	const float InterpolationAlpha = 0.005f;
	const float player1Y = windowY - 60;
	const float player2Y = windowY - 80;

	//enemyVals
	const float enemyRad = 15;
	const sf::Color enemyCol = sf::Color(157, 76, 224);

	//powerUpVals
	const float powerUpRad = 5;
	const sf::Color powerUpColSpeedUp = sf::Color(191, 186, 63);
	const sf::Color powerUpColSpeedDown = sf::Color(255, 153, 63);
	const sf::Color powerUpColSizeUp = sf::Color(13, 188, 168);
	const sf::Color powerUpColSizeDown = sf::Color(13, 112, 188);
	const int powerUpSpawnRate = 80;

	static float powerUpSpeedUpVal = 1.15f;
	static float powerUpSpeedDownVal = 0.85f;
	static float powerUpSizeUpVal = 1.15f;
	static float powerUpSizeDownVal = 0.85f;


	//lobbyValues
	const int lobbyWindowX = 600;
	const int lobbyWindowY = 500;
	const sf::Color clickedColor = sf::Color(60, 60, 60, 255);
	const sf::Color hoveredColor = sf::Color(180, 180, 180, 255);
	
	//Classes
	class MovementQueuer; //forward declarations
	class MovementProcessor;

	class Player //Handles all the data refering to the player, both used in the server and in the client
	{
	public:
		Player();
		~Player();

		//public free access fields
		sf::IpAddress ipAddress;
		unsigned short port;
		int gameID=-1;
		int isConfirmed = 0;
		bool isAlive = true;

		MovementQueuer *inputQueuer = nullptr;

		//accessors for position and id. Position changes the pos var and the drawable pos. ID changes the id and the drawable color.
		sf::Vector2f getPosition();
		sf::Vector2f getPosition() const;
		sf::Vector2f getFollowPosition() const;
		void setPosition(sf::Vector2f _pos);
		void setPositionLerp(float alpha);
		void setFollowablePos(sf::Vector2f _pos);
		void setRandomPosition();


		void setPlayerID(int _id);
		int getPlayerID();
		int getPlayerID() const;
		void setSize(float _newSize);
		void setSpeed(float _newSpeed);

		//Main methods
		void Draw(sf::RenderWindow* _window);
		void Update(float _deltaTime, sf::UdpSocket* _socket);

		Player& operator =(const Player& rhs);
		sf::Color idToColor();

		float sizeScalar = 1;
		float speedScalar = 1;


	private:
		sf::Vector2f pos;
		sf::Vector2f followPos;
		sf::CircleShape drawable;
		int playerID;

		void move(dir _direction, float _deltaTime);
	};
	class MovementQueuer //Queues the orders that the clients generates and merges them in order to send less messages
	{
	public:
		MovementQueuer();
		~MovementQueuer();

		std::queue<Movement> inputQ;
		std::deque<Movement> movementHistory;
		sf::Clock processClock;
		int messageID = 0;
		int gameID = -1;

		void addOrder(Movement _mov);
		void processOrder(sf::UdpSocket* _socket, int _playerID);
		void acknowledgeOrder(bg::Movement _acknowledgedMov, bg::Player* _player);

	};
	class MovementProcessor //Queues the orders that the server receives and merges them by client, so the server has less data to process
	{
	public:
		MovementProcessor();
		~MovementProcessor();

		std::deque<bg::Movement_Server> clientsMoves;
		sf::Clock processClock;

		void addOrder(bg::Movement _mov, int _playerID, sf::IpAddress _ip, unsigned short _port, int _gameID);
		void processOrder(sf::UdpSocket* _socket, std::map<int, bg::Player> *_players, std::map<int, bg::Game> *_games, std::mutex* _mutex);
	};
	class Enemy {
	public:
		Enemy();
		~Enemy();

		float vel=8;

		sf::Vector2f getPos();
		sf::Vector2f getPos() const;
		void setPos(sf::Vector2f _newPos);

		void Update(bg::Game* _room);
		void Draw(sf::RenderWindow* _window);

	private:
		sf::Vector2f pos;
		sf::CircleShape drawable;
	};
	class PowerUp {
	public:
		PowerUp();
		PowerUp(bg::PowerUpType _type);
		~PowerUp();

		float vel = 8;

		sf::Vector2f getPos();
		sf::Vector2f getPos() const;
		void setPos(sf::Vector2f _newPos);

		void Update();
		void Draw(sf::RenderWindow* _window);

		PowerUpType type = PowerUpType::SizeUp;
		sf::CircleShape drawable;

	private:
		sf::Vector2f pos;
	};
	
	
//Graphics paths
#ifdef BUILD
	const std::string LobbyBgPath = "../../deps64/res/lobbyBg.png";
	const std::string LobbyButtonPath = "../../deps64/res/lobbyButton.png";
	const std::string LobbyButtonFont = "../../deps64/res/azonix.otf";
	const std::string LobbyGameLogo = "../../deps64/res/ballGameLogo.png";
	const std::string BufferingPath = "../../deps64/res/buffering.png";
	const std::string IconPath = "../../deps64/res/icon.png";

#endif
#ifndef BUILD
	const std::string LobbyBgPath = "../deps64/res/lobbyBg.png";
	const std::string LobbyButtonPath = "../deps64/res/lobbyButton.png";
	const std::string LobbyButtonFont = "../deps64/res/azonix.otf";
	const std::string LobbyGameLogo = "../deps64/res/ballGameLogo.png";
	const std::string BufferingPath = "../deps64/res/buffering.png";
	const std::string IconPath = "../deps64/res/icon.png";
#endif
}

sf::Packet& operator <<(sf::Packet& packet, const bg::C2SProt& prot);
sf::Packet& operator >>(sf::Packet& packet, bg::C2SProt& prot);

sf::Packet& operator <<(sf::Packet& packet, const bg::S2CProt& prot);
sf::Packet& operator >>(sf::Packet& packet, bg::S2CProt& prot);

sf::Packet& operator <<(sf::Packet& packet, const bg::Movement& mov);
sf::Packet& operator >>(sf::Packet& packet, bg::Movement& mov);

sf::Packet& operator <<(sf::Packet& packet, const bg::Player &player);
sf::Packet& operator >>(sf::Packet& packet, bg::Player &player);

sf::Packet& operator <<(sf::Packet& packet, const bg::Enemy &enemy);
sf::Packet& operator >>(sf::Packet& packet, bg::Enemy &enemy);

sf::Packet& operator <<(sf::Packet& packet, const bg::PowerUp &powerup);
sf::Packet& operator >>(sf::Packet& packet, bg::PowerUp &powerup);

sf::Packet& operator <<(sf::Packet& packet, const bg::PowerUpType& powerUpT);
sf::Packet& operator >>(sf::Packet& packet, bg::PowerUpType& powerUpT);

sf::Packet& operator <<(sf::Packet& packet, const bg::WaitingClient &client);
sf::Packet& operator >>(sf::Packet& packet, bg::WaitingClient &client);