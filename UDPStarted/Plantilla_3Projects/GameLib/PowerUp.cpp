#include "Definition.h"

bg::PowerUp::PowerUp()
{
	drawable = sf::CircleShape(bg::powerUpRad, 5);
	drawable.setFillColor(bg::powerUpColSizeDown); //en functi�n de type
	setPos({ static_cast<float>(rand() % bg::windowX), 10.f });
}

bg::PowerUp::PowerUp(bg::PowerUpType _type) {
	drawable = sf::CircleShape(bg::powerUpRad, 5);
	type = _type;

	switch (_type) {
	case bg::PowerUpType::SizeDown:
		drawable.setFillColor(bg::powerUpColSizeDown); //en functi�n de type
		break;
	case bg::PowerUpType::SizeUp:
		drawable.setFillColor(bg::powerUpColSizeUp); //en functi�n de type
		break;
	case bg::PowerUpType::SpeedUp:
		drawable.setFillColor(bg::powerUpColSpeedUp); //en functi�n de type
		break;
	case bg::PowerUpType::SpeedDown:
		drawable.setFillColor(bg::powerUpColSpeedDown); //en functi�n de type
		break;
	}
	setPos({ static_cast<float>(rand() % bg::windowX), 10.f });
}

bg::PowerUp::~PowerUp()
{
}

sf::Vector2f bg::PowerUp::getPos()
{
	return pos;
}

sf::Vector2f bg::PowerUp::getPos() const
{
	return pos;
}

void bg::PowerUp::setPos(sf::Vector2f _newPos)
{
	pos = _newPos;
	drawable.setPosition(pos);
}

void bg::PowerUp::Update()
{
	setPos({ pos.x, pos.y + vel });

	//Behaviour
}

void bg::PowerUp::Draw(sf::RenderWindow * _window)
{
	_window->draw(drawable);
}

sf::Packet & operator<<(sf::Packet & packet, const bg::PowerUp & powerup)
{
	return packet << powerup.getPos().x << powerup.getPos().y << powerup.type;
}

sf::Packet & operator>>(sf::Packet & packet, bg::PowerUp & powerup)
{
	float posX, posY;
	bg::PowerUpType _type;
	packet >> posX >> posY >> _type;
	powerup.setPos({ posX, posY });
	powerup.type = _type;

	switch (powerup.type) {
	case bg::PowerUpType::SizeDown:
		powerup.drawable.setFillColor(bg::powerUpColSizeDown); //en functi�n de type
		break;
	case bg::PowerUpType::SizeUp:
		powerup.drawable.setFillColor(bg::powerUpColSizeUp); //en functi�n de type
		break;
	case bg::PowerUpType::SpeedUp:
		powerup.drawable.setFillColor(bg::powerUpColSpeedUp); //en functi�n de type
		break;
	case bg::PowerUpType::SpeedDown:
		powerup.drawable.setFillColor(bg::powerUpColSpeedDown); //en functi�n de type
		break;
	}

	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const bg::PowerUpType & powerUpT)
{
	return packet << static_cast<int>(powerUpT);
}

sf::Packet & operator>>(sf::Packet & packet, bg::PowerUpType & powerUpT)
{
	int aux;
	packet >> aux;
	powerUpT = static_cast<bg::PowerUpType>(aux);
	return packet;
}
